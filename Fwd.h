#pragma once

#include <QtCore/QSharedPointer>

namespace QmlScope { namespace Private {

class Scope;
using ScopePtr = QSharedPointer<Scope>;
using ScopeWeakPtr = QWeakPointer<Scope>;

class PropertyInjection;

}}
