#pragma once

#include "Fwd.h"

#include <QtCore/QObject>
#include <QtQml/QQmlParserStatus>

namespace QmlScope {

void import(const char * uri, int versionMajor, int versionMinor);

class Injection
    : public QObject
    , public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)

    Q_PROPERTY(QString scopeName READ scopeName WRITE setScopeName NOTIFY scopeNameChanged)

public:
    explicit Injection(QObject *parent = 0);

    QString scopeName() const { return m_scopeName; }
    void setScopeName(const QString & scopeName);

signals:
    void scopeNameChanged(const QString &);

protected:
    void classBegin() override;
    void componentComplete() override;

private:
    QString m_scopeName;
    Private::PropertyInjection * m_pi {nullptr};
};

}
