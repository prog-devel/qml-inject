import QtQuick 2.0
import Proggy.Scope 1.0

Text {
    // Зачем нам знать, кто и как это устанавливает,
    // просто говорим, что нам нужны такие свойства
    // из определенного скоупа
    Injection {
        id: scope
        scopeName: "Test"

        property QtObject object

        // Если тип не совпадет -- будет ошибка
        //property string object
    }

    text: scope.object.text
}
