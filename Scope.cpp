#include "Scope.h"
#include "ScopePrivate.h"

#include <QtCore/QMetaObject>
#include <QtCore/QMetaMethod>

#include <QtQml>

#include <QDebug>

using namespace QmlScope;
namespace P = Private;

void QmlScope::import(const char * uri, int versionMajor, int versionMinor)
{
    qmlRegisterType<Injection>(uri, versionMajor, versionMinor, "Injection");
}


Injection::Injection(QObject *parent)
    : QObject(parent)
{
}

void Injection::setScopeName(const QString & name)
{
    if (m_scopeName == name) return;
    m_scopeName = name;

    qDebug() << "C++ Scope: name changed" << name;

    if (m_pi) {
        m_pi->stopWatchingAndDelete();
    }

    m_pi = !m_scopeName.isEmpty()
            ? new P::PropertyInjection(this, P::Provider::instance().scope(m_scopeName), {"scopeName"})
            : nullptr;

    emit scopeNameChanged(m_scopeName);
}

void Injection::classBegin()
{

}

void Injection::componentComplete()
{
}
