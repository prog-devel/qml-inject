#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "Scope.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QmlScope::import("Proggy.Scope", 1, 0);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

