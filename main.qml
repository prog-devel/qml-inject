import QtQuick 2.5
import QtQuick.Window 2.2
import Proggy.Scope 1.0

Window {
    visible: true

    Timer {
        id: extObj

        property int counter: 0

        repeat: true
        running: true
        interval: 1000
        onTriggered: counter += 1;
    }

    // Property Injection реализует принцип Dependency Inversion на уровне свойств qml.
    // Это значит, что объекты, которые хотят использовать свойства других объектов,
    // не обязаны от них завасить. Все объекты шарят третью сущность -- Scope,
    // которая реализована на C++ и предостваляется провайдером по имени скоупа
    // каждому инстансу Injection.

    // все свойства, определенные внутри Injection с заданным scopeName
    // можно инжектить в любой другой Injection с таким же scopeName.
    // Причем время их жизни определяется временем жизни последнего Injection
    // в скоупе

    Injection {
        id: baseScope
        scopeName: "Test"

        // если кажется, что прокидывать свойства через Injection -- дорого,
        // то всегда можно прокинуть одно свойство в виде QtObject или Item,
        // и использовать везде уже его свойства.
        // Такой подход будет рациональным при инжекте модулей
        property QtObject object: QtObject {
            property string text: qsTr("Counter: ") + baseScope.prop
        }

        // инжектить можно даже alias-ы
        property alias prop: extObj.counter
    }

    Injection {
        id: baseScope2
        scopeName: "Test2"

        property QtObject object: QtObject {
            property var text: qsTr("Hello Scope2")
        }

        // инжектим из скоупа только явно определенные свойства
        // это значит, что prop в данном случае undefined
//        property int prop: 777
    }

    Injection {
        id: scope
        scopeName: "Test"

        // эти свойства будут неявно взяты из скоупа scopeName.
        // тип свойств имеет значение в той степени, в которой
        // конверитруемы QVariant-ы.
        // лучше, чтобы тип был var, это позволит затаскивать
        // свойства заранее неизвестных типов без оглядки на
        // приведение QVariant к заданному типу.
        property var object
        property var prop

        // и это будет работать, несмотря на то, что свойства
        // явно не меняются.
        onPropChanged: {
            console.log("QML prop changed", prop);
        }
    }

    Column {
        anchors.centerIn: parent
        spacing: 10

        Text {
            text: scope.object.text
        }

        MyItem { }
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onPressed: {
            if ((pressedButtons & Qt.LeftButton)) {
                if (scope.scopeName === "Test") {
                    // протаскиваем изменения свойств везде в рамках скоупа
                    // в данном случае это приведет к сбросу свойства counter у extObj
                    scope.prop = 0;
                }
                else {
                    scope.scopeName = "Test";
                }
            }
            else {
                // легким движением руки меняем все свойства у Injection
                // на свойства другого скоупа
                scope.scopeName = "Test2";
            }
        }
    }
}

