#pragma once

#include "Fwd.h"

#include <QtCore/QObject>
#include <QtCore/QMap>
#include <QtCore/QSet>

namespace QmlScope { namespace Private {

class PropertyWatcher
    : public QObject
{
    Q_OBJECT

public:
    using Filter = QSet<QString>;

    explicit PropertyWatcher(QObject * obj, const Filter & exclude = Filter());

signals:
    void propertyChanged(const QString & name, const QVariant & value);

private:
    Q_SLOT void onPropertyNotify();

private:
    QObject * m_obj{nullptr};
};

class PropertyInjection
    : public QObject
{
    Q_OBJECT

public:
    explicit PropertyInjection(QObject * obj, const ScopePtr & scope, const PropertyWatcher::Filter & exclude = PropertyWatcher::Filter());
    ~PropertyInjection();

    void stopWatchingAndDelete();

private:
    Q_SLOT void onScopedPropertyChanged(const QString & pName, const QVariant & pValue);

private:
    QObject * m_obj;
    ScopePtr m_scope;
    PropertyWatcher * m_w;
};

class Scope
    : public QObject
{
    Q_OBJECT

public:
    explicit Scope(QObject * parent = nullptr);

    Q_SLOT void setScopedProperty(const QString & pName, const QVariant & pValue);

signals:
    void propertyChanged(const QString & pName, const QVariant & pValue);
};

class Provider
    : public QObject
{
    Q_OBJECT

public:
    ScopePtr scope(const QString & scopeName);
    static Provider & instance();

private:
    QMap<QString, ScopeWeakPtr> m_scopes;
};

}}
