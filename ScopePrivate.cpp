#include "ScopePrivate.h"

#include <QtCore/QMetaObject>
#include <QtCore/QMetaMethod>
#include <QtCore/QVariant>

#include <QDebug>

using namespace QmlScope::Private;

PropertyWatcher::PropertyWatcher(QObject * obj, const Filter & exclude)
    : QObject(obj)
    , m_obj(obj)
{
    Q_ASSERT(obj);


    auto notifySlotId = metaObject()->indexOfSlot("onPropertyNotify()");
    Q_ASSERT(notifySlotId >= 0);
    auto notifySlot = metaObject()->method(notifySlotId);
    Q_ASSERT(notifySlot.isValid());

    const auto meta = m_obj->metaObject();
    Q_ASSERT(meta);

    for (auto i = meta->propertyOffset(); i < meta->propertyCount(); ++i) {
        auto prop = meta->property(i);

        if (exclude.contains(prop.name())) continue;

        auto signalMethod = prop.notifySignal();
        if (signalMethod.isValid()) {
            connect(m_obj, signalMethod, this, notifySlot);
        }

        // trick: обращаемся к свойству
        // это заставит создать lazy connection для alias-свойств
        // где-то в недрах qmlcore
        prop.write(m_obj, prop.read(m_obj));
    }
}

void PropertyWatcher::onPropertyNotify()
{
    Q_ASSERT(m_obj == sender());

    auto signalId = senderSignalIndex();
    Q_ASSERT(signalId >= 0);

    const auto meta = m_obj->metaObject();
    Q_ASSERT(meta);

    for (auto i = meta->propertyOffset(); i < meta->propertyCount(); ++i) {
        auto prop = meta->property(i);

        auto signalMethod = prop.notifySignal();
        if (signalMethod.methodIndex() == signalId) {
            emit propertyChanged(prop.name(), prop.read(m_obj));
            return;
        }
    }
}


PropertyInjection::PropertyInjection(QObject * obj, const ScopePtr & scope, const PropertyWatcher::Filter & exclude)
    : QObject(obj)
    , m_obj(obj)
    , m_scope(scope) // shared ref ++
    , m_w(new PropertyWatcher(obj, exclude))
{
    Q_ASSERT(obj && scope);
    m_w->setParent(this);

    // инжектим из private scope только явно определенные в obj свойства
    auto meta = obj->metaObject();
    for (auto i = meta->propertyOffset(); i < meta->propertyCount(); ++i) {
        auto localProp = meta->property(i);
        Q_ASSERT(localProp.isValid());
        const auto pName = localProp.name();
        if (exclude.contains(pName)) continue;

        auto localValue = localProp.read(obj);
        auto remoteValue = scope->property(pName);

        // свойства obj имеют приоритет над свойствами private scope
        if (!localValue.isNull() && remoteValue.isNull()) {
            qDebug() << "C++ Scope: Store prop to remote" << pName << localValue;
            scope->setScopedProperty(pName, localValue);
        }
        // если свойствио obj == null, то инжектим свойство из private scope
        else if (!remoteValue.isNull()) {
            qDebug() << "C++ Scope: Init prop from remote" << pName << remoteValue;
            localProp.write(obj, remoteValue);
        }
    }

    // все изменения свойств obj инжектим в private scope
    connect(m_w, &PropertyWatcher::propertyChanged, scope.data(), &Scope::setScopedProperty);

    // все изменения свойств private scope инжектим в obj
    // XXX Здесь не лямбда. Чтобы дисконнектить на удалении this
    connect(scope.data(), &Scope::propertyChanged, this, &PropertyInjection::onScopedPropertyChanged);
}

PropertyInjection::~PropertyInjection()
{
}

void PropertyInjection::stopWatchingAndDelete()
{
    // сразу разрываем все коннекшны, чтобы не было гонок
    delete m_w;
    m_w = nullptr;

    disconnect(m_scope.data(), nullptr, this, nullptr);
    deleteLater();
}

void PropertyInjection::onScopedPropertyChanged(const QString & pName, const QVariant & pValue)
{
    const auto pCName = pName.toLocal8Bit();
    // не инжектим свойства "задним числом"
    if (m_obj->metaObject()->indexOfProperty(pCName) < 0) return;

    // не зацикливаемся при установке свойств private scope
    if (m_obj->property(pCName) == pValue) return;

    qDebug() << "C++ Scope: private property changed" << pName << pValue << m_obj;

    // не зацикливаемся при установке собственных свойств obj
    const QSignalBlocker sblocker(m_w);
    m_obj->setProperty(pCName, pValue);
}


Scope::Scope(QObject * parent)
    : QObject(parent)
{
}

void Scope::setScopedProperty(const QString & pName, const QVariant & pValue)
{
    qDebug() << "C++ P::Scope: set property" << pName << pValue;
    const auto pCName = pName.toLocal8Bit();
    if (property(pCName) == pValue) return;
    setProperty(pCName, pValue);
    emit propertyChanged(pName, pValue);
}

Provider & Provider::instance()
{
    static Provider s_p;
    return s_p;
}

ScopePtr Provider::scope(const QString & scopeName)
{
    ScopePtr scope;

    auto it = m_scopes.find(scopeName);
    if (it == m_scopes.end()) {
        scope = ScopePtr::create();
        connect(scope.data(), &QObject::destroyed, [this, scopeName]() {
            m_scopes.remove(scopeName);
        });
    }
    else {
        scope = it.value().toStrongRef();
    }

    m_scopes[scopeName] = scope.toWeakRef();

    return scope;
}
